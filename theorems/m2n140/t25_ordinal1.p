fof(t25_ordinal1, conjecture,  (! [A] :  ~ ( (! [B] :  (r2_hidden(B, A) <=> v3_ordinal1(B)) ) ) ) ).
fof(t24_ordinal1, axiom,  (! [A] :  ~ ( ( (! [B] :  (r2_hidden(B, A) => v3_ordinal1(B)) )  &  (! [B] :  (v3_ordinal1(B) =>  ~ (r1_tarski(A, B)) ) ) ) ) ) ).
fof(t5_ordinal1, axiom,  (! [A] :  (! [B] :  ~ ( (r2_hidden(A, B) & r1_tarski(B, A)) ) ) ) ).
